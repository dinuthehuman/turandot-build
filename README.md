# Turandot Build

Building Turandot as deb and eventually as msi

## Linux

### Install node.js dependencies (Linux/Debian)

```bash
sudo apt-get install nodejs npm
sudo npm install -g pkg
```
### Tested nodejs build commands

```bash
npm install .
pkg --target node8-linux-x64  --output ./linux_x64 .
pkg --target node8-win-x64 --output ./windows_x64.exe .
pkg --target node8-macos-x64 --output ./macos_x64 .
```

### More agnostic command set:

```bash
pkg --target node8-linux  --output ./citeproc-js-server .
pkg --target node8-win-x64 --output ./citeproc-js-server.exe .
```

## Windows: Notes

### Install PyGObject on MSYS2:

- Go to http://www.msys2.org/ and download the x86_64 installer
- Follow the instructions on the page for setting up the basic environment
- Run `C:\msys64\mingw64.exe` - a terminal window should pop up
- Execute `pacman -Suy`
- Execute `pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-python3 mingw-w64-x86_64-python3-gobject`
- To test that GTK 3 is working you can run `gtk3-demo`
- Copy the `hello.py` script you created to `C:\msys64\home\<username>`
- In the mingw32 terminal execute `python3 hello.py` - a window should appear.

[Source](https://pygobject.readthedocs.io/en/latest/getting_started.html#windows)

### Probably useful

- [StackOverflow: PyInstaller should work on MSYS2](https://stackoverflow.com/questions/54728231/how-to-fix-pyinstaller-in-msys2-mingw-your-platform-is-not-yet-supported)
- [gitmemory.com: About the "Platform not supported" Error](https://www.gitmemory.com/issue/pyinstaller/pyinstaller/4570/565179561)
