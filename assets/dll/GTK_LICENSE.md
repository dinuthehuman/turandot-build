GTK is released under the terms of the GNU Lesser General Public License, version 2.1 or, at your option, any later version, as published by the Free Software Foundation.

Please, see the [COPYING](https://gitlab.gnome.org/GNOME/gtk/-/blob/master/COPYING) file for further information.
